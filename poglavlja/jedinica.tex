\section{Mikroprogramirana upravljačka jedinica}

Razmotrimo, konačno, upravljačku jedinicu procesora. Središnja komponenta mikroprogramirane upravljačke jedinice je mikroprogramska (ili upravljačka) memorija, koja sadrži mikroinstrukcije (ta memorija nalazi se u samom procesoru, u njegovoj upravljačkoj jedinici, i nema nikakve veze s glavnom memorijom računala). Kao što je već rečeno, svakoj strojnoj instrukciji odgovara jedan niz mikroinstrukcija u mikroprogramskoj memoriji – mikroprogram. Adresa početka mikroprograma jednoznačno je određena na temelju operacijskog koda makroinstrukcije.

Svaka mikroinstrukcija pak nije ništa drugo nego jedan dugački niz bitova, grupiranih u određena polja, pri čemu svako polje upravlja jednim upravljačkim signalom ili grupom upravljačkih signala i tako pobuđuje mikrooperacije u procesoru.

Na slici~\ref{fig:control} prikazana je struktura mikroprogramirane upravljačke jedinice procesora koji se koristi u ovoj laboratorijskoj vježbi.

\begin{figure}[ht]
\centering
\includegraphics[width=0.8\textwidth]{./figures/control_unit.png} 
\caption{\textbf{Mikroprogramirana upravljačka jedinica}}
\label{fig:control}
\end{figure}


Pri vrhu slike vidimo mikroprogramsku memoriju (``micro memory"). Kao i kod svake druge memorije, da bi se pročitao sadržaj određene memorijske lokacije, potrebno je specificirati adresu s koje želimo čitati (oznaka ``next\_address" na slici~\ref{fig:control}, gore lijevo). Adresiranjem mikroprogramske memorije, na njenim podatkovnim linijama pojavljuje se mikroinstrukcija pohranjena na toj adresi (još se naziva i mikroriječ). Ona se sastoji od većeg broja polja bitova, što na slici~\ref{fig:control} odgovara strelicama s natpisima koje izlaze iz mikroprogramske memorije. Opišimo ukratko ulogu pojedinih grupa bitova u mikroinstrukciji, no radi lakšeg praćenja, opisivat ćemo ih malo drukčijim redoslijedom nego što se nalaze na slici~\ref{fig:control}.

Krenimo najprije od četiri krajnje desne grupe signala (četiri krajnje desne linije koje izlaze iz mikroprogramske memorije na slici~\ref{fig:control}). Ove četiri grupe bitova odnose se na određivanje adrese sljedeće mikroinstrukcije. Naime, za razliku od makroinstrukcija, koje se u pravilu izvode slijedno jedna za drugom (o čemu se brine programsko brojilo), kod mikroinstrukcija nije unaprijed osigurano slijedno izvođenje, nego svaka mikroinstrukcija eksplicitno mora specificirati adresu sljedeće mikroinstrukcije. Da bi se, međutim, omogućilo i uvjetno grananje unutar mikroprograma, u razmatranom modelu procesora mikroinstrukcija sadrži dva polja za adresu sljedeće mikroinstrukcije: address\_true (adresa sljedeće mikroinstrukcije ako je uvjet ispunjen) i address\_false (adresa sljedeće mikroinstrukcije ako uvjet nije ispunjen), te dvobitno polje cond koje služi za specifikaciju uvjeta o kojemu ovisi hoće li adresa sljedeće mikroinstrukcije biti određena jednim ili drugim adresnim poljem. Bezuvjetno grananje, koje nam je potrebno ćešće od uvjetnog, ostvaruje se tako da se oba polja address\_true i address\_false postave na istu vrijednost, uz proizvoljnu vrijednost polja cond. Sam uvjet može biti jedan od četiri moguća, kao što je vidljivo sa slike~\ref{fig:control} (gdje se vidi da polje cond upravlja multipleksorom 4/1): može ovisiti o rezultatu aritmetičke ili logičke operacije (m7 – najviši bit rezultata je 1, odnosno rezultat je negativan; c\_out – pri rezultatu se pojavio prijenos; v - pojavio se aritmetički preljev), te o stanju memorije (wait – memorijska operacija još nije završila). Konačno, posljednje polje za specifikaciju adrese sljedeće mikroinstrukcije je jednobitno polje index\_sel, koje, kada je postavljeno, specificira da se adresa sljedeće mikroinstrukcije ne određuje na temelju polja address\_true i address false, nego na temelju operacijskog koda makroinstrukcije u instrukcijskom registru.

Uloga preostalih signala i signalnih grupa mikroinstrukcije jasna je već iz slike~\ref{fig:datapath} – oni naime izravno odgovaraju pojedinim upravljačkim signalima koji upravljaju odgovarajućim, već ranije opisanim elementima puta podataka:


\renewcommand{\labelenumi}{(\roman{enumi})}

\begin{itemize}
	\item Signali rj\_sel i rk\_sel te dva trobitna polja (grupe signala) a\_sel i b\_sel specificiraju koji od registara će biti postavljeni na sabirnice operanada (a\_bus i b\_bus – ulazi u ALU, vidi sliku~\ref{fig:datapath}). Ove registre moguće je, naime, specificirati na dva načina:
		\begin{enumerate}
			\item Izravno iz mikroinstrukcije upravljačkim signalima a\_sel i b\_sel, te

			\item Iz makroinstrukcije (strojne instrukcije), na temelju polja ir\_rj (bitovi ) i ir\_rk u instrukcijskom registru (ovim načinom moguće je specificirati samo registre r0 – r3, jer u makroinstrukciji imamo na raspolaganju samo po dva bita za specifikaciju registara).
		\end{enumerate}

Koji od ovih dvaju načina će se koristiti pri izvođenju dane mikroinstrukcije, određeno je dodatnim upravljačkim bitovima rj\_sel (za prvi operand) i rk\_sel (za drugi operand). Ako su ovi bitovi u 0, koristi se način (i); ako su pak postavljeni u 1, koristi se način (ii). O ovoj funkcionalnosti brinu se dva multipleksora koja vidimo na samom vrhu slike~\ref{fig:datapath}, iznad skupa registara.

\item Trobitna grupa signala alu\_sel odabire jednu od osam mogućih funkcija ALU.

\item Upravljački signal c\_in postavlja u 0 ili 1 ulaz c\_in u ALU.

\item Dvobitni signal result\_sel upravlja multipleksorom koji određuje što će se pojavitina sabirnici rezultata (result\_bus) radi upisa u odredišni registar. Na slici~\ref{fig:datapath} uočavamo da se o tome brine multipleksor 4/1 koji, ovisno o vrijednosti polja resutl\_sel predviđa četiri mogućnosti što će se pojaviti na sabirnici rezultata: (a) rezultat aritmetičke ili logičke operacije iz ALU (koji se nalazi na sabirnici alu\_bus), (b) sadržaj memorijskog podatkovnog registra (MDR), odnosno podatak pročitan iz memorije, (c) 4-bitna usputna konstanta iz instrukcijskog registra, predznačno proširena na 8 bita, te (d) 8-bitna usputna konstanta iz instrukcijskog registra.

\item Signali ri\_sel te r0\_write – r7\_write omogućuju upis podatka sa sabirnice rezultata u registre iz skupa r0 – r7. Ako je aktivan signal ri\_sel, odredišni registar je određen na temelju dvobitnog polja ri u instrukcijskom registru, odnosno makroinstrukciji (na ovaj način ponovno je moguće pristupiti samo registrima r0 – r3), a ako je signal ri\_sel neaktivan, odredišne registre određuju signali r0\_write – r7\_write. Pošto je riječ o osam nezavisnih signala koje je moguće aktivirati istovremeno i neovisno jedne o drugima, moguće je isti podatak sa sabirnice rezultata istovremeno upisati i u više registara (iako je to rijetko kada korisno).

\item Signal mar\_sel aktivira upis podataka sa sabirnice alu\_bus (izlaz iz ALU) u memorijski adresni registar (MAR).

\item Signal mdr\_sel je dvobitni signal koji aktivira upis u memorijski podatkovni registar (MDR). Podatak koji se upisuje u MDR može biti ili podatak sa sabirnice alu\_bus ili podatak s vanjske podatkovne sabirnice procesora (podatak iz memorije).

\item Signali ir1\_sel i ir0\_sel aktiviraju upis podatka s vanjske podatkovne sabirnice (iz memorije) u viši, odnosno niži bajt instrukcijskog registra. 

\item Signali read i write upućuju se memoriji i aktiviraju čitanje, odnosno pisanje.
\end{itemize}
