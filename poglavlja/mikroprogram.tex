\section{Pisanje mikroprograma}

Mikroprogrami su nizovi mikroinstrukcija, pri čemu su pojedine mikroinstrukcije nizovi bitova u skladu s formatom opisanim u prethodnom poglavlju.

No da bi se mikroprogrameru olakšao posao, mikroprogramirani sustavi u pravilu ne zahtijevaju da se mikroprogrami pišu izravno na razini pojedinačnih bitova. Umjesto toga, većina sustava podržava unos mikroprograma u nekom simboličkom formatu, a sam sustav se brine o prevođenju takvog simboličkog formata u pojedinačne bitove mikroinstrukcije. Takav je slučaj i u simulatoru MythSim.

Mikroprogram za simulator MythSim piše se u obliku obične tekstualne datoteke (korištenjem bilo kojeg uređivača teksta – npr. notepad, gedit, vi, ...) u skladu sa sljedećim pravilima:

\begin{itemize}
\item Naziv datoteke ima ekstenziju .ucode.
\item Komentari počinju oznakom // i protežu se do kraja retka.
\item Svaka mikroinstrukcija može biti označena labelom, da bi se omogućilo grananje na tu naredbu. Labela se nalazi na početku retka i odvojena je od ostatka mikroinstrukcije dvotočkom. Npr:
\begin{lstlisting}
labela: //  ovdje slijedi nastavak mikroinstrukcije ...
\end{lstlisting}

\item Mikroinstrukcije se međusobno odvajaju oznakom ; (točka-zarez).
\item Pojedina mikroinstrukcija se sastoji od niza oznaka koje predstavljaju upravljačke signale. Oznake su međusobno odvojene zarezima. Ako je riječ o jednobitnim signalima, samo navođenje imena signala specificira njegovo postavljanje u 1, a izostavljanje tog imena u mikroinstrukciji označava da signal poprima vrijednost 0. Npr., ako se u liniji koja odgovara mikroinstrukciji nalazi oznaka c\_in, znači da će signal c\_in biti postavljen u 1. Ako nema oznake c\_in u mikroinstrukciji, taj signal će biti u 0. Ako je riječ o višebitnim signalima, koristi se sintaksa \texttt{ime=vrijednost}, pri čemu se vrijednost može pisati u simboličkom obliku. Npr. \texttt{alu\_sel=ADD} znači da će selekcijski ulazi aritmetičko-logičke jedinice biti postavljeni na trobitnu vrijednost koja odgovara operaciji zbrajanja (korisnik ne mora znati koja je to točno vrijednost, nego koristi simbolički naziv ADD). Primjer jedne mikroinstrukcije:
\begin{lstlisting}
labela: ri_sel, rj_sel, rk_sel, alu_sel=ADD;
\end{lstlisting}

To znači: signali ri\_sel, rj\_sel i rk\_sel će biti postavljeni u 1, a signal alu\_sel u vrijednost koja odgovara operaciji zbrajanja. Svi ostali (jednobitni i višebitni) signali koji nisu navedeni u mikroinstrukciji poprimit će vrijednost 0. Npr. ako je signal result\_sel izostavljen, pretpostavlja se \texttt{result\_sel=ALU}. \textbf{Popis svih upravljačkih polja u mikroinstrukciji, njihovih značenja i vrijednosti koje mogu poprimiti dan je u tabličnom obliku u dodatku na kraju ovih uputa.}
\item Za specificiranje adrese sljedeće mikroinstrukcije koristi se simbolička oznaka ``goto \textit{labela}". Ako ovakve oznake u mikroinstrukciji nema, simulator podrazumijeva da je sljedeća mikroinstrukcija ona koja se nalazi neposredno iza tekuće u tekstualnoj datoteci.
\item Za ostvarivanje uvjetnog grananja koristi se sintaksa 
\begin{lstlisting}
if signal_uvjeta then goto labela endif; ili
if signal_uvjeta then goto labela1 else goto labela2 endif;
\end{lstlisting}
\item Dopuštene su prazne linije između mikroinstrukcija.
\end{itemize}

\renewcommand{\labelenumi}{\arabic{enumi}.}
Tipični mikroprogram sastojat će se od tri dijela, kao što ćemo pokazati na primjeru malo niže:
\begin{enumerate}
	\item Dio koji se odnosi na fazu ``pribavi" nalazi se uvijek na početku .ucode datoteke.
	\item Slijedi dio koji se odnosi na operacijske kodove pojedinih instrukcija (``opcode" dio), pri čemu svakoj instrukciji (svakom operacijskom kodu) odgovara točno jedna linija (tj. jedna mikroinstrukcija) u ovom dijelu datoteke.
	\item Ako mikroprogram za neku strojnu instrukciju zahtijeva više od jedne mikroinstrukcije, ostale mikroinstrukcije (osim prve) moraju se nalaziti ispod ``opcode" dijela koja sadrži po jednu liniju za svaki operacijski kod. Ovaj treći dio naziva se ``opcode extension" dio. Prva mikroinstrukcija (koja se nalazi u dijelu (2), tj. ``opcode" dijelu) mora u tom slučaju sadržavati ``goto" na odgovarajuću labelu gdje se nalazi nastavak mikroprograma.
\end{enumerate}

To da nastavci mikroprograma moraju biti ispod svih prvih mikroinstrukcija, proizlazi iz arhitekture mikroprorgamirane upravljačke jedinice, odnosno načina određivanja adrese početka mikroprograma na temelju operacijskog koda makroinstrukcije. To vidimo iz slike 2: ako je aktivan signal index\_sel, operacijski kod smješten u instrukcijskom registru se pribraja adresi sljedeće mikroinstrukcije. To znači da se prva mikroinstrukcija za strojnu naredbu s operacijskim kodom 0 mora nalaziti neposredno iza zadnje mikroinstrukcije za fazu pribavi, prva mikroinstrukcija za strojnu naredbu s operacijskim kodom 1 točno jednu adresu iza mikroinstrukcije za strojnu naredbu s operacijskim kodom 0 itd..

Primjer mikroprograma:

\begin{lstlisting}
// ====================== PRIBAVI =======================

fetch0: a_sel=7, b_sel=7, alu_sel=OR, mar_sel=LOAD;		// MAR <- PC
fetch1: ir1_sel=LOAD, read, if wait then goto fetch1 endif;	// IR_high <- MEM(MAR)
fetch2: a_sel=7, c_in, alu_sel=ADDA, r7_write;			// PC <- PC+1
fetch3: a_sel=7, b_sel=7,  alu_sel=OR,  mar_sel=LOAD;		// MAR <- PC
fetch4: ir0_sel=LOAD, read, if wait then goto fetch4 endif;	// IR_low <- MEM(MAR)
fetch5: a_sel=7, c_in, alu_sel=ADDA, r7_write, goto opcode[IR_OPCODE];	// PC <- PC+1

// ============= DIO OPERACIJSKIH KODOVA =============

// 0) NOP
opcode[0]: goto fetch0;

// 1) LOAD_IMMEDIATE (ri <- ir_const8)
opcode[1]: result_sel=IR_CONST8, ri_sel, goto fetch0;

// 2) ADD (ri <- rj + rk)
opcode[2]: ri_sel, rj_sel, rk_sel, alu_sel=ADD, 
	if m_7 then goto opcode2.1 else goto opcode 2.2 endif;

// 3) HALT
opcode[3]: goto opcode[3];

// ================= DIO EKSTENZIJE ================
// postavi zastavicu N
opcode2.1: a_sel=4, b_sel=4, alu_sel=XOR, r4_write; // pomocni registar r4 <- 0
	a_sel=4, c_in, alu_sel=ADDA, r6_write, goto fetch0; // r4=0 + c_in=1 -> r6 (SR)

// obrisi zastavicu N
opcode2.2: a_sel=4, b_sel=4; alu_sel=XOR, r4_write; // pomocni registar r4 <- 0
	a_sel=4, alu_sel=ADDA, r6_write, goto fetch0; // r4=0 -> r6 (SR)
\end{lstlisting}


Prvih šest mikroinstrukcija (označenih labelama fetch0 – fetch5) odgovara fazi ``pribavi". Prve tri mikroinstrukcije čitaju prvi (viši) bajt makroinstrukcije u instrukcijski registar, dok druge tri čitaju niži bajt (ovdje pretpostavljamo da procesor koristi big endian način zapisivanja podataka; primjeri koji dolaze uz simulator pretpostavljaju little endian način).

Promotrimo pobliže mikrokod za fazu ``pribavi".

Prva mikroinstrukcija (fetch0) prebacuje sadržaj programskog brojila u memorijski adresni registar. Kako procesor nema posebnog registra za programsko brojilo, mi kao programsko brojilo koristimo registar r7. No s obzirom na strukturu puta podataka, sadržaj bilo kojeg registra mora najprije proći kroz ALU, kako bi završio na sabirnici alu\_bus, prije nego što ga je moguće upisati u MAR. Da bi sadržaj registra r7 pri tom prolasku kroz ALU ostao nepromijenjen, mi koristimo jednostavan ``trik": specificiramo operaciju OR (logičko ILI) i dovodimo na oba ulaza ALU sadržaj registra r7. Logičko ili nekog binarnog broja samog sa sobom daje isti taj broj, pa će se na izlazu iz ALU pojaviti nepromijenjen sadržaj registra r7 (alternativno, mogli smo odabrati i operaciju AND, ili čak ADDA uz c\_in=0, odnosno SUBA uz c\_in=1; rezulat bi bio isti).

Druga mikroinstrukcija (fetch1) aktivira čitanje iz memorije (signal read) i upisuje pročitani podatak u viši bajt instrukcijskog registra (ir1\_sel=LOAD). Sama operacija čitanja može zahtijevati više vremena, jer je memorija tipično sporija od procesora. Zbog toga druga mikroinstrukcija ostvaruje petlju: ona provjerava stanje signala wait kojim memorija signalizira je li operacija pristupa memoriji završena ili još traje. Ako memorija još nije završila operaciju (signal wait je aktivan), mikroinstrukcija skače sama na sebe i tako se vrti u petlji sve dok pristup memoriji ne završi. Kao što je napomenuto na kraju odjeljka~\ref{sec:model}, signal wait se aktivira tijekom istog perioda takta, pa procesor neće prijeći na treću mikroinstrukciju prije nego što memorija stvarno dojavi da je postavila vrijednost na sabirnicu.

Treća mikroinstrukcija (fetch2) uvećava programsko brojilo (tj. registar r7) za 1, kako bi bio spreman za adresiranje sljedećeg bajta strojne instrukcije. U tu svrhu na jedan ulaz ALU (a\_bus) postavlja se sadržaj registra r7 i aktivira se operacija ADDA uz c\_in=1. Svejedno je što se nalazi na drugom ulazu ALU (b\_bus), jer operacija ADDA djeluje samo nad jednim operandom. Tako uvećani sadržaj sprema se nazad u programsko brojilo (r7\_write).

Preostale tri mikroinstrukcije za fazu ``pribavi" (fetch3 – fetch5) su identične strukture kao i prve tri, samo s tom razlikom da učitavaju drugi bajt instrukcije u niži bajt instrukcijskog registra (ir0\_sel=LOAD). Jedina značajnija razlika nalazi se na samom kraju posljednje instrukcije faze pribavi, a sastoji se u tome da ova posljednja naredba ima dio ``goto opcode[IR\_OPCODE]". To zapravo znači da će tijekom izvođenja te mikroinstrukcije biti aktivan signal index\_sel, koji će daljnje izvođenje mikroprograma usmjeriti ovisno o operacijskom kodu pročitane makroinstrukcije.

Preostale mikroinstrukcije odgovaraju fazi ``izvrši" četiriju strojnih instrukcija (makroinstrukcija): NOP, LOAD\_IMMEDIATE, ADD i HALT. Tri od ove četiri strojne instrukcije imaju samo jednu mikroinstrukciju; jedino instrukcija ADD ima nastavak u dijelu ekstenzije (ovaj nastavak odnosi se samo na postavljanje zastavice N – najnižeg bita u registru r6, koji koristimo kao statusni registar). Objašnjenja pojedinih mikroinstrukcija dana su u komentarima uz same mikroinstrukcije, iz čega bi trebala biti jasna njihova funkcionalnost. Zadnja mikroinstrukcija svakog mikroprograma za fazu ``izvrši" sadrži oznaku goto fetch0, čime se osigurava ponovni prelazak procesora u fazu ``pribavi" nakon dovršetka faze ``izvrši" za trenutnu strojnu instrukciju.

