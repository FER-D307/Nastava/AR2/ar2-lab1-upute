\section{Uvod}
Predmet ove laboratorijske vježbe je mikroprogramiranje. Mikroprogramiranje je jedna od dvaju osnovnih tehnika izvedbe upravljačke jedinice procesora.

Da bi nam bilo jasnije o čemu se radi, trebamo najprije nešto znati o unutarnjoj građi procesora. Procesor, kao središnja komponenta računala, interno se sastoji od nekoliko komponenata: skupa registara, aritmetičko-logičke jedinice, upravljačke jedinice, te veza među njima. Razmotrimo ukratko uloge pojedinih komponenata. Uloga registara je privremeno pohranjivanje podataka, a aritmetičko-logička jedinica (ALU) omogućuje obavljanje različitih jednostavnih aritmetičkih i logičkih operacija nad podatcima iz registara. Registri i ALU međusobno su povezani jednom ili većim brojem spojnih puteva, tzv. \textit{internih sabirnica}. Procesor će morati omogućiti i vezu prema glavnoj memoriji i možebitnim drugim komponentama računala, a ona će biti ostvarena putem posebnih registara, koji su spojeni na vanjske, adresne i podatkovne, izvode procesora. Aritmetičko-logička jedinica, registri i interne sabirnice koje ih povezuju, često se nazivaju zajedničkim imenom \textit{put podataka}.

Ovom osnovnom strukturom procesora određene su neke elementarne operacije koje je procesor u stanju izravno, na najnižoj sklopovskoj razini podržati. Npr:
\begin{itemize}
	\item postavljanje podatka iz jednog registra na internu sabirnicu;
	\item aktiviranje određene aritmetičke ili logičke operacije u ALU;
	\item upis podatka s interne sabirnice u neki od registara;
	\item aktiviranje čitanja podatka iz memorije, ili upisa podatka u memoriju.
\end{itemize}

Ovakve elementarne, izravno sklopovski podržane operacije, nazivaju se \textit{mikrooperacijama}.

Rad procesora svodi se na dohvaćanje i izvršavanje strojnih instrukcija, koje su predstavljene numeričkim kodom i pohranjene u glavnoj memoriji. Iako su sa stajališta programera strojne instrukcije vrlo jednostavne, štoviše, najelementarnije moguće instrukcije, sa stajališta procesora one su relativno složene i često ih procesor neće moći izvršiti u jednom koraku nego će ih morati razložiti na niz elementarnih koraka koji su izravno sklopovski podržani, odnosno na niz mikrooperacija, u odgovarajućem vremenskom redoslijedu. Npr., strojna instrukcija ADD A,B,C (zbroji sadržaje registara B i C i rezultat spremi u registar A) u tipičnom se procesoru ne može izvesti u jednom koraku, nego se mora razložiti na sljedeće mikrooperacije:
\begin{itemize}
	\item postavi sadržaj registra A na internu sabirnicu koja je spojena na jedan ulaz ALU;
	\item postavi sadržaj registra B na internu sabirnicu koja je spojena na drugi ulaz ALU;
	\item aktiviraj operaciju zbrajanja u ALU (ovisno o rezultatu operacije postavi i odgovarajuće zastavice procesora, obično su to Z (nula), V (preljev), C (prijenos), N (negativan broj));
	\item podatak s interne sabirnice koja je spojena na izlaz iz ALU upiši u registar C.
\end{itemize}

Svakoj mikrooperaciji u procesoru je pridružen odgovarajući \textit{upravljački signal} koji njome upravlja (aktiviranje odnosno deaktiviranje upravljačkog signala aktivira odnosno deaktivira odgovarajuću mikrooperaciju). Procesor dakle mora, na temelju saznanja o tome koja strojna instrukcija se treba izvršiti, generirati odgovarajući vremenski slijed upravljačkih signala. Zadaća generiranja slijeda upravljačkih signala na temelju operacijskog koda tekuće strojne instrukcije spada na \textit{upravljačku jedinicu} procesora.

Kao što je već napomenuto, dva su osnovna načina realizacije upravljačke jedinice: sklopovski i mikroprogramski.

\begin{enumerate}
\item Ako je upravljačka jedinica sklopovski realizirana, ona se promatra kao sekvencijski digitalni sklop – crna kutija koja na ulazu prima operacijski kod tekuće instrukcije i sistemski takt, a na izlazu generira upravljačke signale u odgovarajućem vremenskom slijedu. Ovako izvedena upravljačka jedinica projektira se standardnim metodama koje se u digitalnoj logici koriste za projektiranje sekvencijskih digitalnih sklopova. Prednosti ovakve (sklopovske) izvedbe upravljačke jedinice su manje zauzeće površine na čipu, manja potrošnja i veća brzina rada, no nedostatak joj je u tome što je za bilo kakvu promjenu u jednom realiziranoj sklopovskoj upravljačkoj jedinici potreban potpuni redizajn.

\item Alternativni pristup izvedbi upravljačke jedinice predstavlja mikroprogramiranje. Kod mikroprogramski izvedene upravljačke jedinice, svaka strojna instrukcija predstavljena je nizom tzv. \textit{mikroinstrukcija}. Ove mikroinstrukcije smještene su u posebnoj memoriji, tzv. \textit{mikroprogramskoj} ili \textit{upravljačkoj memoriji}, koja se nalazi unutar same upravljačke jedinice procesora (u stvarnim mikroprogramiranim procesorima to je tipično memorija tipa ROM).Svaka mikroinstrukcija specificira upravljačke signale koje je u nekom trenutku potrebno aktivirati. Niz mikroinstrukcija tvori tzv. \textit{mikroprogram}. Svakoj strojnoj instrukciji odgovara, dakle, jedan mikroprogram smješten u mikroprogramskoj memoriji. Na temelju operacijskog koda trenutne strojne instrukcije, upravljačka jedinica “zna” gdje se u mikroprogramskoj memoriji nalazi mikroprogram koji odgovara baš toj strojnoj instrukciji, te ga izvršava. Strojne instrukcije nazivaju se u ovom kontekstu \textit{makroinstrukcijama}, a programi sačinjeni od strojnih instrukcija nazivaju se \textit{makroprogramima}. Korištenjem mikroprogramiranja, projektant procesora može realizirati vlastiti skup strojnih instrukcija. Koncept mikroprogramiranja detaljnije je opisan u predavanjima i u knjizi S.Ribarić, Građa računala, Algebra, Zagreb, 2011 (poglavlja 7.2 (str. 163-165) i 7.4, (str. 187-200)).
\end{enumerate}

U ovoj vježbi studenti će realizirati vlastiti skup strojnih instrukcija za procesor zadane interne organizacije. Na taj način steći će osjećaj za rad procesora na najnižoj razini. Pošto nam stvarni mikroprogramirljivi procesor nije na raspolaganju, vježba će se obaviti korištenjem simulatora \textbf{MythSim}, razvijenog na University of Illinois, Chicago, USA. Simulator je pisan u programskom jeziku Java i može se pokretati na svim raširenijim operacijskim sustavima. Do simulatora se može doći
% na stranici \href{http://www.mythsim.org/}{\bf{http://www.mythsim.org/}} ili 
na internim stranicama kolegija Arhitektura računala 2 (FER web).



