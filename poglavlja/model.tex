\section{Model mikroprogramiranog procesora} \label{sec:model}

Kao što smo vidjeli u uvodu, projektant mikroprogramirane upravljačke jedinice mora za svaku strojnu instrukciju (makroinstrukciju) napisati odgovarajući mikroprogram. Mikroprogram je niz mikroinstrukcija koje će se izvesti jedna za drugom, a svaka mikroinstrukcija specificira skup upravljačkih signala koji će biti aktivirani u trenutku njezina izvođenja u svrhu pobuđivanja odgovarajućih mikrooperacija. Iz ovoga je jasno da projektant – mikroprogramer mora najprije znati kakve mikrooperacije njegov procesor podržava, odnosno mora detaljno poznavati internu organizaciju procesora, da bi onda mogao svaku makroinstrukciju razložiti na odgovarajući slijed mikrooperacija i na temelju njih napisati mikroprogram.


Kao što smo također vidjeli u uvodu, općenito govoreći put podataka procesora sastoji se od skupa registara, aritmetičko-logičke jedinice i odgovarajućeg broja internih sabirnica koje ih povezuju. No koliko točno ima registara, koliko su oni široki (koliko bitova imaju), koliko ima internih sabirnica, koje operacije podržava ALU, ovisi o konkretnom procesoru koji se koristi.

\begin{figure}[ht]
\centering
\includegraphics[width=0.8\textwidth]{./figures/datapath.png} 
\caption{\textbf{Model mikroprogramiranog procesora}} \label{fig:datapath}
\end{figure}


Model mikroprogramiranog procesora koji se koristi u simulatoru MythSim, odnosno njegovog puta podataka, prikazan je na slici~\ref{fig:datapath}. Njegove značake su sljedeće:
\begin{enumerate}
\item \textbf{Skup registara}. Procesor koristi skup od 8 8-bitnih registara opće namjene. Ti registri nazivaju se imenima r0 – r7. Na slici~\ref{fig:datapath} ovi registri prikazani su kućicom označenom kao ``register file" u gornjem lijevom dijelu slike. Osim ovih osam registara, procesor još posjeduje 8-bitni memorijski adresni registar (MAR) i 8-bitni memorijski registar podataka (MDR) koji služe za komunikaciju s memorijom, te 16-bitni instrukcijski registar (IR) koji sadrži trenutnu strojnu instrukciju. Ova posljednja tri registra prikazana su pri dnu slike~\ref{fig:datapath}.

Primijetimo da procesor ne raspolaže posebnim registrom za programsko brojilo. Pošto je programsko brojilo neizostavno potrebno za realizaciju procesora von Neumannove arhitekture, bit će potrebno jedan od registara opće namjene r0 – r7 (tipično r7, ali projektant može odlučiti i drukčije) koristiti kao programsko brojilo. Isto razmatranje vrijedi i za statusni registar i kazalo stoga -- u osnovnom putu podataka nema registara koji bi odgovarali ovim ulogama, te, ako su nam takvi registri potrebni, moramo u tu svrhu koristiti neki od registara iz registarskog skupa r0 – r7.

\item \textbf{Aritmetičko-logička jedinica}. Procesor raspolaže 8-bitnom ALU (na slici~\ref{fig:datapath} desno od skupa registara) koja podržava 8 aritmetičkih i logičkih operacija. Podržane operacije navedene su u tablici u desnom gornjem kutu slike. Neke operacije (konkretno, logička operacija NOT te aritmetičke operacije ADDA i SUBA) djeluju samo nad jednim operandom, dok ostale djeluju na dva operanda. Odabir konkretne aritmetičke ili logičke operacije koju ALU izvodi u određenom trenutku vrši se trobitnim upravljačkim signalom alu\_sel. ALU također ima i dodatni upravljački signal c\_in (ulazni bit prijenosa) koji se koristi u aritmetičkim operacijama i koji se može postaviti u 0 ili 1. Izlazi iz ALU su: 8-bitni rezultat aritmetičke ili logičke operacije (izlaz alu\_bus) te bitovi c\_out (engl. carry out - prijenos s najvišeg bita),
%; važna napomena: u simulatoru MythSim izlaz c\_out poprima invertiranu vrijednost u odnosu na stvarni prijenos (ako prijenosa nema, vrijednost izlaza je 1, ako ga ima, vrijednost je 0))
v (engl. overflow – aritmetički preljev) te m7 (najznačajniji bit rezultata, koji predstavlja informaciju o predznaku rezultata).

\item \textbf{Interne sabirnice}. Procesor koristi tri interne sabirnice (trosabirnička arhitektura): dvije sabirnice operanda – na slici označene kao a\_bus i b\_bus (između skupa registara i ALU) i jednu sabirnicu rezultata - na slici označena kao result\_bus (ulaz u skup registara). Tijekom svakog procesorskog takta, na sabirnice operanada postavljaju se sadržaji određenih registara iz skupa r0 - r7. Odabir konkretnih registara čiji će sadržaji biti postavljeni na sabirnice operanada vrši se upravljačkim signalima a\_sel, b\_sel, rj\_sel i rk\_sel. Točna semantika ovih signala i način njihove uporabe bit će objašnjeni kasnije, u poglavlju o strukturi upravljačke jedinice. 

Tijekom svakog procesorskog takta moguće je upisati sadržaj sa sabirnice rezultata u jedan ili više registara iz skupa r0 – r7. Odabir takvih odredišnih registara obavlja se upravljačkim signalima ri\_sel, te r0\_write – r7\_write. Podatak koji se nalazi na sabirnici rezultata može biti rezultat aritmetičke ili logičke operacije (izlaz iz ALU) ili podatak iz memorijskog registra podataka, ili pak konstanta ( 8-bitna ili predznačno prširena 4-bitna) sadržana u instrukcijskom registru kao sastavni dio instrukcije (tzv. usputna konstanta). Odabir jedne od ovih četiriju mogućnosti vrši se pomoću multipleksora kojim upravlja dvobitni upravljački signal result\_sel.

\item \textbf{Veza s memorijom}. Veza s memorijom ostvarena je preko već spomenutih registara MAR (memorijski adresni registar, spojen na adresnu sabirnicu) i MDR (memorijski podatkovni registar, spojen na podatkovnu sabirnicu), te upravljačkih signala read, write i wait.

\begin{enumerate}
	\item Operacija čitanja. Prilikom operacije čitanja, procesor mora postaviti adresu ciljne memorijske lokacije u MAR, te aktivirati signal read. Memoriji je potrebno određeno vrijeme prije nego što odgovori i postavi podatak na podatkovnu sabirnicu procesora (vrijeme pristupa),  koje procesoru nije unaprijed poznato. Stoga memorija drži aktivnim signal wait sve dok pristup podatku nije završen, nakon čega postavlja podatak na podatkovnu sabirnicu i deaktivira signal wait. Deaktiviranje signala wait znak je procesoru da je traženi podatak postavljen na podatkovnu sabirnicu i da ga procesor može preuzeti. Primijetimo da podatak koji je memorija postavila na podatkovnu sabirnicu prilikom operacije čitanja, procesor može upisati u memorijski registar podataka MDR (aktiviranjem signala mdr\_sel) ili u viši ili niži bajt instrukcijskog registra (aktiviranjem upravljačkih signala ir1\_sel, odnosno ir0\_sel).

	\item Operacija pisanja. Prilikom operacije pisanja, procesor postavlja i adresu ciljne memorijske lokacije u MAR, i podatak u MDR, te aktivira signal write. Memorija drži aktivnim signal wait sve dok podatak nije upisan, signalizirajući time procesoru koliko dugo traje operacija pisanja. Za razliku od čitanja, upis podatka u memoriju moguće je jedino preko registra MDR.
\end{enumerate}

Ako je trenutna mikrooperacija aktivirala signal read (ili write), a odabir sljedeće mikrooperacije ovisi o signalu wait, pretpostavljamo da će se signal wait aktivirati tijekom istog perioda takta.

Organizacija procesora osigurava da se isti registar može koristiti i kao izvorni i kao odredišni operand, bez pojave nedeterminizma. Za potrebe zadovoljenja znatiželje bistrih studenata ovdje ćemo navesti dva načina da se to provede u praksi. Prvi način se temelji na generatoru slijedova uz $k=2$. Čitanje sabirnice operanada i upisivanje u interni registar ALU provelo bi se u $\varphi_1$, dok bi se upisivanje preko sabirnice rezultata provelo u $\varphi_2$. U drugom načinu elemente registarskog skupa izveli bismo bridom okidanim bistabilima. Upisivanje vrijednosti proveli bismo na brid signala takta koji odgovara kraju instrukcijskog ciklusa.

\end{enumerate}


